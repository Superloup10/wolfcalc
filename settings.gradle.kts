pluginManagement {
    repositories {
        maven {setUrl("https://artifactory.wolfdev.fr/libs-release/")}
    }
}

rootProject.name = "WolfCalc"
