import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.61"
    id("org.jlleitschuh.gradle.ktlint") version "9.1.1"
    id("io.gitlab.arturbosch.detekt") version "1.2.2"
    id("org.sonarqube") version "2.8"
    id("com.github.hierynomus.license") version "0.15.0"
    id("org.jetbrains.dokka") version "0.10.0"
    application
    jacoco
}

group = "fr.wolfdev"
version = "1.0-SNAPSHOT"
application.mainClassName = "$group.app.${rootProject.name}"

apply(from = "gradle/license.gradle")
apply(from = "gradle/sonar.gradle")

repositories {
    maven {setUrl("https://artifactory.wolfdev.fr/libs-release/")}
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("no.tornado:tornadofx:1.7.19")
    implementation("de.jensd:fontawesomefx-commons:8.14")
    implementation("de.jensd:fontawesomefx-fontawesome:4.7.0-3")
    implementation("org.kordamp.bootstrapfx:bootstrapfx-core:0.2.4")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.0")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.5.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.0")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}

ktlint {
    ignoreFailures.set(true)
    additionalEditorconfigFile.set(file(".editorconfig"))
}

detekt {
    toolVersion = "1.2.2"
    input = files("src/main/kotlin")
    config = files("detekt-config.yml")
    reports {
        xml {
            enabled = true
            destination = file("$buildDir/reports/detekt/detekt.xml")
        }
    }
}

tasks.compileKotlin {
    dependsOn(tasks.ktlintFormat)
}

tasks.jacocoTestReport {
    sourceDirectories.from("src/main/kotlin")
}

sonarqube {
    properties {
        detekt.reports.xml.destination?.let {property("sonar.kotlin.detekt.reportPaths", it)}
    }
}

tasks.jacocoTestReport {
    dependsOn(detekt)
}

tasks.check {
    dependsOn(tasks.jacocoTestReport)
}

tasks.withType<DokkaTask> {
    outputFormat = "html"
    outputDirectory = "$buildDir/dokka"
}
